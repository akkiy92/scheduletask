# ScheduleTask App

### Installation

ScheduleTask requires [Node.js](https://nodejs.org/) v10.13.0 to run.
Additional Requirements : - Node - Postman Client - Nodemon - Nest using Express - Agenda Lib | Node - MongoDB

Install the Mongodb Server

```sh
$ ./mongod
Verify that mongodb server has been started
```

Update the Application Properties as per the system

```sh
$ cd ./src/Assets/Properties/dev.json
{
    "portNumber": 3050,
    "dbConfig": {
        "client": "mongodb",
        "host": "127.0.0.1",
        "username": "",
        "password": "",
        "database": "schedule",
        "port": 27017
    },
    "jobsSchema": "email_jobs",
    "emailConfig": {
        "host": "smtp.mailtrap.io",
        "port": 587,
        "username": "0f5a75d861e3d2",
        "password": "da9ab6fb1d7cezh"
    },
    "apiUsername" : "api",
    "apiPassword" : "api"
}
Update the properties as per your sytem configuration.
```

Install the dependencies and devDependencies and start the server.

```sh
$ cd AgendaTask
$ npm install
$ Run using : npm run start:PROFILE_NAME
$ npm run start:dev        |  Run server for dev in watch mode
$ npm run start:nodemon    |  Run server for dev with nodemon in debug and watch mode
```

### API

#### Create Task

Request:

```sh
URL :  http://localhost:3050/v1/scheduletask
Method : POST
AUthorization :  Basic
```

```sh
{
	"name" : "Email Job Task",
	"key" : "EJT-1",
	"isActive" : true,
	"isRepeating" : false,
	"pattern" : "1 minutes",
	"emailContent" : {
		"attendees" : [],
		"subject" : "mailakkiy@gmail.com",
		"content" : "Welcome here!!!"
	}
}
```

Response

```sh
{
    "name" : "Email Job Task",
	"key" : "EJT-1",
	"isActive" : true,
	"isRepeating" : false,
	"pattern" : "1 minutes",
	"emailContent" : {
		"attendees" : [],
		"subject" : "mailakkiy@gmail.com",
		"content" : "Welcome here!!!"
	}
}
```

#### Update Task

Request:

```sh
URL :  http://localhost:3050/v1/scheduletask/:key
Method : PUT
Authorization :  Basic
```

```sh
 {
	"name" : "Email Job Task",
	"key" : "EJT-1",
	"isActive" : true,
	"isRepeating" : false,
	"pattern" : "1 minutes",
	"emailContent" : {
		"attendees" : [],
		"subject" : "mailakkiy@gmail.com",
		"content" : "Welcome here!!!"
	}
}
```

Response

```sh
{
    "name" : "Email Job Task",
	"key" : "EJT-1",
	"isActive" : true,
	"isRepeating" : false,
	"pattern" : "1 minutes",
	"emailContent" : {
		"attendees" : [],
		"subject" : "mailakkiy@gmail.com",
		"content" : "Welcome here!!!"
	}
}
```

#### Get Task By Key

Request:

```sh
URL :  http://localhost:3050/v1/scheduletask/:key
Method : GET
Authorization :  Basic
```

Response

```sh
{
    "name" : "Email Job Task",
	"key" : "EJT-1",
	"isActive" : true,
	"isRepeating" : false,
	"pattern" : "1 minutes",
	"emailContent" : {
		"attendees" : [],
		"subject" : "mailakkiy@gmail.com",
		"content" : "Welcome here!!!"
	}
}
```

#### Get All Task

Request:

```sh
URL :  http://localhost:3050/v1/scheduletask
Method : GET
Authorization :  Basic
```

Response

```sh
[
    {
        "name" : "Email Job Task",
    	"key" : "EJT-1",
    	"isActive" : true,
    	"isRepeating" : false,
    	"pattern" : "1 minutes",
    	"emailContent" : {
    		"attendees" : [],
    		"subject" : "mailakkiy@gmail.com",
    		"content" : "Welcome here!!!"
    	}
    }
]
```

#### Delete Task

Request:

```sh
URL :  http://localhost:3050/v1/scheduletask/:key
Method : DELETE
Authorization :  Basic
```

Response

```sh
{
    "message": "Deleted Successfully."
}
```

## License

Unlicensed
