import { AbstractException } from './AbstractException';

export class ValidationException extends AbstractException {
    private statusCode: number = 400;
    private errors: CustomValidationError[] = [];
    public getStatusCode(): number {
        return this.statusCode;
    }

    public getValidationErrors(): CustomValidationError[] {
        return this.errors;
    }

    constructor(message, errors: any[]) {
        super(message);
        this.setValidationErrors(errors);
    }

    private setValidationErrors(errors) {
        for (let error of errors) {
            const validationError = new CustomValidationError(error.property, error.constraints);
            if (error.children != null) {
                for (let childError of error.children) {
                    validationError.pushChildError(new CustomValidationError(childError.property, childError.constraints));
                }
            }
            this.errors.push(validationError);
        }
    }
}

class CustomValidationError {
    private children: CustomValidationError[] = [];
    private property: string;
    private constraints: any[];

    public constructor(property, constraints) {
        this.property = property;
        this.constraints = constraints;
    }

    public pushChildError(error: CustomValidationError) {
        this.children.push(error);
    }
}
