export abstract class AbstractException extends Error {
  public abstract getStatusCode(): number;
}
