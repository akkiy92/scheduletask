import {
  ExceptionFilter,
  ArgumentsHost,
  Catch,
  HttpStatus,
} from '@nestjs/common';
import { ValidationException } from '../ValidationException';
import { GenericResponse, ErrorResponse } from 'src/Core/Http/GenericResponse';
import { Response } from 'express';

@Catch(ValidationException)
export class ValidationExceptionHandler implements ExceptionFilter {
  catch(exception: ValidationException, host: ArgumentsHost) {
    const err: ValidationException = exception;
    const message = 'Validation Exception';
    const errorMessageDetails = err.getValidationErrors();
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    return response
      .status(400)
      .send(
        new GenericResponse(
          new ErrorResponse(message, 400, errorMessageDetails),
          undefined,
          400,
        ),
      );
  }
}
