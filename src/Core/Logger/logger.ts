import { createLogger, format, transports } from 'winston';
import * as path from 'path';
import fs from 'fs';

class Logger {
  private static format = format.printf(info => {
    if (info.stack) {
      return `${info.timestamp} ${info.level} ${info.label} : ${info.stack}`;
    }
    return `${info.timestamp} ${info.level} ${info.label} : ${info.message}`;
  });

  public static getLogger(T: any) {
    const logger = new Logger();
    return logger.initializeLogger(T);
  }

  private initializeLogger(T: any) {
    return createLogger({
      level: 'debug',
      format: format.combine(
        format.errors({ stack: true }),
        format.colorize(),
        format.simple(),
        format.label({ label: T.name }),
        format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss',
        }),
        Logger.format,
      ),
      transports: [
        new transports.Console({
          level: 'debug',
          format: format.combine(
            format.errors({ stack: true }),
            format.colorize(),
            format.simple(),
            format.label({ label: T.name }),
            format.timestamp({
              format: 'YYYY-MM-DD HH:mm:ss',
            }),
            Logger.format,
          ),
        }),
        new transports.File({
          filename: 'application.log',
          format: format.combine(
            format.errors({ stack: true }),
            format.colorize(),
            format.simple(),
            format.label({ label: T.name }),
            format.timestamp({
              format: 'YYYY-MM-DD HH:mm:ss',
            }),
            Logger.format,
          ),
        }),
      ],
    });
  }
}

export class LoggerFactory {
  public static getLogger(T: any) {
    return Logger.getLogger(T);
  }
}
