import { v4 as uuidv4 } from 'uuid';

export class ErrorResponse {
  public message: any;
  public messageDetails: any;
  public code: string;
  public errorId: string;

  constructor(message, code, details?) {
    this.message = message;
    this.code = code;
    this.errorId = uuidv4();
    this.messageDetails = details;
  }
}

export class GenericResponse {
  public error: ErrorResponse;
  public statusCode: number;
  public result: any;
  private timestamp: string;
  constructor(error: ErrorResponse, result: any, statusCode: number) {
    this.error = error;
    this.statusCode = statusCode;
    this.result = result;
    this.timestamp = new Date().toISOString();
  }
}
