import { NestInterceptor, ExecutionContext, CallHandler, Injectable, ForbiddenException, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Request } from 'express';
import { AppPropertiesFactory } from 'src/Constant/app.properties.factory';

@Injectable()
export class AuthInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const username = AppPropertiesFactory.getProperties().apiUsername || 'api';
        const password = AppPropertiesFactory.getProperties().apiPassword || 'api';
        const auth = 'Basic ' + new Buffer(username + ':' + password).toString('base64');
        const request: Request = context.getArgByIndex(0);
        if (request.headers.authorization != auth) {
            throw new UnauthorizedException('Basic Authorization is required.');
        }
        return next.handle().pipe(tap(() => {}));
    }
}
