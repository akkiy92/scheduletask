import * as path from 'path';
import fse from 'fs-extra';
import os from 'os';

export class FileUtils {
  public static removeAllSpecialCharacterFromFileName(stringToReplace) {
    let tmp = stringToReplace.substring(0, stringToReplace.lastIndexOf('.'));
    tmp = tmp.replace(/[^\w\s]/gi, '');
    tmp = tmp.replace(/ /g, '');
    tmp =
      tmp +
      stringToReplace.substring(
        stringToReplace.lastIndexOf('.'),
        stringToReplace.length,
      );
    return tmp;
  }

  public static getTempDir() {
    return os.tmpdir();
  }

  public static getBaseDir() {
    const baseDir = path.dirname(require.main.filename);
    return baseDir.substring(0, baseDir.lastIndexOf('/dist'));
  }

  public static getSourceDir() {
    return FileUtils.getBaseDir() + '/src';
  }
}
