import { NestFactory } from '@nestjs/core';
import 'reflect-metadata';
import { ValidationExceptionHandler } from 'src/Core/Exception/Handler/ValidationExceptionHandler';
import { AppModule } from './AppStarter/app.module';
import { AppPropertiesFactory } from './Constant/app.properties.factory';
import { AgendaFactory } from './Modules/Agenda/agenda.factory';
import { AppContextManager } from './Constant/app.context.manager';
import { AppJobsManager } from './AppStarter/app.jobsmanager';
import { AuthInterceptor } from './Core/Interceptor/request.interceptor';

/**
 * This will start the Application
 * @class AppStarter
 */
class AppStarter {
    private port: Number = AppPropertiesFactory.getProperties().portNumber;
    constructor() {}

    public async initialize() {
        AppContextManager.app = await NestFactory.create(AppModule);
        await AppContextManager.app.listen(this.port.toString());
        new AppJobsManager().resetJobs();
    }
}

const appStarter = new AppStarter();
appStarter.initialize();
