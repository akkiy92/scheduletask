import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailModule } from 'src/Modules/Email/email.module';
import { EmailService } from 'src/Modules/Email/email.service';

@Module({
  imports: [EmailModule],
  providers: [EmailService],
})
export class EmailJobModule {}
