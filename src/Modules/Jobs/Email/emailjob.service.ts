import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AgendaFactory } from 'src/Modules/Agenda/agenda.factory';
import { LoggerFactory } from 'src/Core/Logger/logger';
import { EmailService } from 'src/Modules/Email/email.service';
import { EmailContent } from 'src/Modules/Email/email.dto';

@Injectable()
export class EmailJobService {
    private retryMaxAttempt: number = 5;
    private agenda = AgendaFactory.getEmailAgenda();
    private logger = LoggerFactory.getLogger(EmailJobService);

    constructor(private emailService: EmailService) {}

    public async addJob(pattern: string, taskKey: string, isRepeatingEvent: boolean, emailContent: EmailContent) {
        this.logger.info('Adding Email Job with key : ' + taskKey);
        this.defineEmailTask(taskKey, 'high');
        const weeklyReport = this.agenda.create(taskKey);
        if (isRepeatingEvent) {
            await this.agenda.every(pattern, taskKey, emailContent);
        } else {
            await this.agenda.schedule(pattern, taskKey, emailContent);
        }
    }

    public async cancelJob(taskKey: string) {
        this.logger.info('Cancelling Email Job with key : ' + taskKey);
        await this.agenda.cancel({ name: taskKey });
    }

    public async purgeAll() {
        this.logger.info('Purging all Email Jobs ... ');
        await this.agenda.purge();
    }

    private async sendEmail(emailContent: EmailContent) {
        await this.emailService.sendEmail(emailContent.getAttendees(), emailContent.getContent(), emailContent.getSubject());
    }

    private async defineEmailTask(taskKey: string, priority: string) {
        let failCount: number = 0;
        this.agenda.define(taskKey, { priority: priority, concurrency: 10 }, async (job) => {
            this.logger.info('Running Email Task [' + taskKey + '] ...');
            if (job.attrs.data == null) {
                this.logger.warn('No Email Content found for Job with key : [' + taskKey + ']');
                return;
            }
            await this.sendEmail(new EmailContent().deserialize(job.attrs.data));
        });
        this.agenda.on('fail:' + taskKey, (err, job) => {
            if (failCount >= this.retryMaxAttempt) {
                this.logger.info('Not Reattempting Task [' + taskKey + '] on failure. Max Retry Attemp Reached ...');
            }
            failCount++;
            this.logger.info('Reattempting Task [' + taskKey + '] on failure ...');
            job.schedule('now').save();
        });
    }
}
