import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EmailService } from '../Email/email.service';
import { EmailJobModule } from '../Jobs/Email/emailjob.module';
import { EmailJobService } from '../Jobs/Email/emailjob.service';
import { ScheduleTaskController } from './emailscheduletask.controller';
import { EmailScheduleTask } from './emailscheduletask.entity';
import { EmailScheduleTaskService } from './emailscheduletask.service';

@Module({
    imports: [EmailJobModule, TypeOrmModule.forFeature([EmailScheduleTask])],
    providers: [EmailScheduleTaskService, EmailJobService, EmailService],
    controllers: [ScheduleTaskController],
})
export class ScheduleTaskModule {}
