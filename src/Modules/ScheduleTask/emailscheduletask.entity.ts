import {
  ObjectIdColumn,
  ObjectID,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EmailScheduleTaskDTO } from './emailscheduletask.dto';
import { EmailContent } from '../Email/email.dto';

@Entity()
export class EmailScheduleTask {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  private name: string;

  @Column()
  private key: string;

  @Column()
  private description: string;

  @Column()
  private isActive: boolean;

  @Column()
  private isRepeating: boolean;

  @Column()
  private pattern: string;

  @Column()
  private emailContent: EmailContent;

  public getEmailContent(): EmailContent {
    return this.emailContent;
  }

  public setEmailContent(emailContent: EmailContent): void {
    this.emailContent = emailContent;
  }

  public getPattern(): string {
    return this.pattern;
  }

  public setPattern(pattern: string): void {
    this.pattern = pattern;
  }

  public isIsRepeating(): boolean {
    return this.isRepeating;
  }

  public setIsRepeating(isRepeating: boolean): void {
    this.isRepeating = isRepeating;
  }

  public getKey(): string {
    return this.key;
  }

  public setKey(key: string): void {
    this.key = key;
  }

  public getId(): ObjectID {
    return this.id;
  }

  public setId(id: ObjectID): void {
    this.id = id;
  }

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getDescription(): string {
    return this.description;
  }

  public setDescription(description: string): void {
    this.description = description;
  }

  public isIsActive(): boolean {
    return this.isActive;
  }

  public setIsActive(isActive: boolean): void {
    this.isActive = isActive;
  }

  public static getEntityFromDTO(dto: EmailScheduleTaskDTO): EmailScheduleTask {
    if (dto == null) {
      return null;
    }
    const entity = new EmailScheduleTask();
    entity.setDescription(dto.getDescription());
    entity.setIsActive(dto.isIsActive());
    entity.setKey(dto.getKey());
    entity.setName(dto.getName());
    entity.setIsRepeating(dto.isIsRepeating());
    entity.setPattern(dto.getPattern());
    entity.setEmailContent(dto.getEmailContent());
    return entity;
  }
}
