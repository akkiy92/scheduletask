import { IsNotEmpty, MaxLength, IsOptional, IsBoolean, ValidateNested, IsNotEmptyObject } from 'class-validator';
import { EmailScheduleTask } from './emailscheduletask.entity';
import { Expose, Type } from 'class-transformer';
import { EmailContent } from '../Email/email.dto';

export class EmailScheduleTaskDTO {
    @Expose()
    private id: string;

    @Expose()
    @IsNotEmpty({ message: 'Name is required.' })
    @MaxLength(50, { message: 'Name can be of maximum 50 character.' })
    private name: string;

    @Expose()
    @IsNotEmpty({ message: 'Key is required.' })
    @MaxLength(50, { message: 'Key can be of maximum 20 character.' })
    private key: string;

    @Expose()
    @IsOptional()
    @MaxLength(255, { message: 'Description can be of maximum 255 character.' })
    private description: string;

    @Expose()
    @IsBoolean({ message: 'isActive should be a boolean value.' })
    private isActive: boolean;

    @Expose()
    @IsBoolean({ message: 'isRepeating should be a boolean value.' })
    private isRepeating: boolean;

    @Expose()
    @MaxLength(255, { message: 'Pattern can be of maximum 255 character.' })
    @IsNotEmpty({ message: 'Pattern is required.' })
    private pattern: string;

    @Expose()
    @IsNotEmptyObject({ message: 'EmailContent is required.' })
    @ValidateNested()
    @Type(() => EmailContent)
    private emailContent: EmailContent;

    public getEmailContent(): EmailContent {
        return this.emailContent;
    }

    public setEmailContent(emailContent: EmailContent): void {
        this.emailContent = emailContent;
    }

    public deserialize(dto: any) {
        Object.assign(this, dto);
        this.emailContent = new EmailContent().deserialize(dto.emailContent);
        return this;
    }

    public getId(): string {
        return this.id;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getKey(): string {
        return this.key;
    }

    public setKey(key: string): void {
        this.key = key;
    }

    public getDescription(): string {
        return this.description;
    }

    public setDescription(description: string): void {
        this.description = description;
    }

    public isIsActive(): boolean {
        return this.isActive;
    }

    public setIsActive(isActive: boolean): void {
        this.isActive = isActive;
    }

    public isIsRepeating(): boolean {
        return this.isRepeating;
    }

    public setIsRepeating(isRepeating: boolean): void {
        this.isRepeating = isRepeating;
    }

    public getPattern(): string {
        return this.pattern;
    }

    public setPattern(pattern: string): void {
        this.pattern = pattern;
    }

    public static getDTOFromEntity(entity: EmailScheduleTask): EmailScheduleTaskDTO {
        if (entity == null) {
            return null;
        }
        const dto = new EmailScheduleTaskDTO();
        dto.setDescription(entity.getDescription());
        dto.setIsActive(entity.isIsActive());
        dto.setIsRepeating(entity.isIsRepeating());
        dto.setKey(entity.getKey());
        dto.setName(entity.getName());
        dto.setPattern(entity.getPattern());
        dto.setId(entity.getId().toString());
        dto.setEmailContent(entity.getEmailContent());
        return dto;
    }
}
