import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EmailJobService } from '../Jobs/Email/emailjob.service';
import { EmailScheduleTaskDTO } from './emailscheduletask.dto';
import { EmailScheduleTask } from './emailscheduletask.entity';

@Injectable()
export class EmailScheduleTaskService {
    constructor(
        @InjectRepository(EmailScheduleTask)
        private readonly taskRepository: Repository<EmailScheduleTask>,
        private emailJobService: EmailJobService
    ) {}

    async createScheduleTask(scheduleTaskDTO: EmailScheduleTaskDTO): Promise<EmailScheduleTaskDTO> {
        const key = scheduleTaskDTO.getKey();
        let scheduleTask: EmailScheduleTask = new EmailScheduleTask();
        scheduleTask.setKey(key);
        scheduleTask.setIsActive(true);
        scheduleTask = await this.taskRepository.findOne(scheduleTask);
        if (scheduleTask != null) {
            throw new BadRequestException('Task already exist with the given key.');
        }
        await this.emailJobService.addJob(
            scheduleTaskDTO.getPattern(),
            key,
            scheduleTaskDTO.isIsRepeating(),
            scheduleTaskDTO.getEmailContent()
        );
        scheduleTask = EmailScheduleTask.getEntityFromDTO(scheduleTaskDTO);
        scheduleTask = await this.taskRepository.save(scheduleTask);
        return EmailScheduleTaskDTO.getDTOFromEntity(scheduleTask);
    }

    async updateScheduleTask(scheduleTaskDTO: EmailScheduleTaskDTO): Promise<EmailScheduleTaskDTO> {
        const key = scheduleTaskDTO.getKey();
        let scheduleTask: EmailScheduleTask = new EmailScheduleTask();
        scheduleTask.setKey(key);
        scheduleTask.setIsActive(true);
        scheduleTask = await this.taskRepository.findOne(scheduleTask);
        if (scheduleTask == null) {
            throw new BadRequestException('Task not found with the given key.');
        }
        await this.emailJobService.cancelJob(key);
        await this.emailJobService.addJob(
            scheduleTaskDTO.getPattern(),
            key,
            scheduleTaskDTO.isIsRepeating(),
            scheduleTaskDTO.getEmailContent()
        );
        scheduleTask.setDescription(scheduleTaskDTO.getDescription());
        scheduleTask.setEmailContent(scheduleTaskDTO.getEmailContent());
        scheduleTask.setIsRepeating(scheduleTaskDTO.isIsRepeating());
        scheduleTask.setName(scheduleTaskDTO.getName());
        scheduleTask.setPattern(scheduleTaskDTO.getPattern());
        scheduleTask = await this.taskRepository.save(scheduleTask);
        return EmailScheduleTaskDTO.getDTOFromEntity(scheduleTask);
    }

    async getScheduleTask(taskKey: string): Promise<EmailScheduleTaskDTO> {
        let scheduleTask: EmailScheduleTask = new EmailScheduleTask();
        scheduleTask.setKey(taskKey);
        scheduleTask.setIsActive(true);
        scheduleTask = await this.taskRepository.findOne(scheduleTask);
        if (scheduleTask == null) {
            throw new NotFoundException('Task not found with the given key.');
        }
        return EmailScheduleTaskDTO.getDTOFromEntity(scheduleTask);
    }

    async deleteScheduleTask(taskKey: string): Promise<EmailScheduleTaskDTO> {
        let scheduleTask: EmailScheduleTask = new EmailScheduleTask();
        scheduleTask.setKey(taskKey);
        scheduleTask.setIsActive(true);
        scheduleTask = await this.taskRepository.findOne(scheduleTask);
        if (scheduleTask == null) {
            throw new NotFoundException('Task not found with the given key.');
        }
        scheduleTask.setIsActive(false);
        scheduleTask = await this.taskRepository.save(scheduleTask);
        // --------------- Cancel the Job --------------- //
        await this.emailJobService.cancelJob(taskKey);
        return EmailScheduleTaskDTO.getDTOFromEntity(scheduleTask);
    }

    async getAllScheduleTask(): Promise<EmailScheduleTaskDTO[]> {
        let scheduleTask: EmailScheduleTask = new EmailScheduleTask();
        scheduleTask.setIsActive(true);
        const scheduleTasks: EmailScheduleTask[] = await this.taskRepository.find(scheduleTask);
        const resultDTO: EmailScheduleTaskDTO[] = [];
        for (let task of scheduleTasks) {
            resultDTO.push(EmailScheduleTaskDTO.getDTOFromEntity(task));
        }
        return resultDTO;
    }
}
