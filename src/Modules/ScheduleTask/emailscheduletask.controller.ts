import { Controller, Get, Post, UseFilters, Body, Param, Delete, HttpCode, Put, UseInterceptors } from '@nestjs/common';
import { EmailScheduleTaskService } from './emailscheduletask.service';
import { ValidationExceptionHandler } from 'src/Core/Exception/Handler/ValidationExceptionHandler';
import { ValidationService } from 'src/Common/Services/ValidationService';
import { EmailScheduleTaskDTO } from './emailscheduletask.dto';
import { AuthInterceptor } from 'src/Core/Interceptor/request.interceptor';

@Controller('/v1/scheduletask')
@UseInterceptors(new AuthInterceptor())
export class ScheduleTaskController {
    constructor(private readonly emailScheduleTaskService: EmailScheduleTaskService) {}

    @Post()
    @HttpCode(200)
    @UseFilters(new ValidationExceptionHandler())
    async createJob(@Body() scheduleTask: EmailScheduleTaskDTO): Promise<EmailScheduleTaskDTO> {
        scheduleTask = new EmailScheduleTaskDTO().deserialize(scheduleTask);
        await ValidationService.validateEntity(scheduleTask);
        return await this.emailScheduleTaskService.createScheduleTask(scheduleTask);
    }

    @Put(':key')
    @HttpCode(200)
    @UseFilters(new ValidationExceptionHandler())
    async updateJob(@Param('key') taskKey, @Body() scheduleTask: EmailScheduleTaskDTO): Promise<EmailScheduleTaskDTO> {
        scheduleTask = new EmailScheduleTaskDTO().deserialize(scheduleTask);
        await ValidationService.validateEntity(scheduleTask);
        scheduleTask.setKey(taskKey);
        return await this.emailScheduleTaskService.updateScheduleTask(scheduleTask);
    }

    @Get(':key')
    async findByKey(@Param('key') taskKey): Promise<EmailScheduleTaskDTO> {
        return await this.emailScheduleTaskService.getScheduleTask(taskKey);
    }

    @Get()
    async findAll(): Promise<EmailScheduleTaskDTO[]> {
        return await this.emailScheduleTaskService.getAllScheduleTask();
    }

    @Delete(':key')
    @HttpCode(200)
    async remove(@Param('key') key: string) {
        await this.emailScheduleTaskService.deleteScheduleTask(key);
        return { message: 'Deleted Successfully.' };
    }
}
