import { Injectable } from '@nestjs/common';
import * as nodeMailer from 'nodemailer';
import { AppPropertiesFactory } from 'src/Constant/app.properties.factory';
import { LoggerFactory } from 'src/Core/Logger/logger';

@Injectable()
export class EmailService {
    private static logger = LoggerFactory.getLogger(EmailService);
    private transporter: any;
    private mailOptions = {
        from: '"Task"<donotreply@task.in>',
        to: [],
        html: '',
        subject: '',
    };

    constructor() {
        this.initialize();
    }

    /**
     * @private
     * @memberof Mailer
     */
    private initialize() {
        this.transporter = nodeMailer.createTransport({
            host: AppPropertiesFactory.getProperties().emailConfig.host,
            port: AppPropertiesFactory.getProperties().emailConfig.port,
            secure: false,
            auth: {
                user: AppPropertiesFactory.getProperties().emailConfig.username,
                pass: AppPropertiesFactory.getProperties().emailConfig.password,
            },
        });
    }

    /**
     * @param {string[]} toAddress
     * @param {string} message
     * @param {string} subject
     * @returns
     * @memberof EmailService
     */
    public sendEmail(toAddress: string[], message: string, subject: string) {
        return new Promise((resolve, reject) => {
            this.mailOptions.to = toAddress;
            this.mailOptions.html = message;
            this.mailOptions.subject = subject;
            this.transporter.sendMail(this.mailOptions, (error, info) => {
                if (error) {
                    EmailService.logger.error('Error Occurred While Sending Email ...');
                    EmailService.logger.error(error.message);
                    reject(error);
                } else {
                    resolve(info);
                }
            });
        });
    }
}
