import { Expose } from 'class-transformer';
import { IsNotEmpty, MaxLength } from 'class-validator';

export class EmailContent {
  @Expose()
  @IsNotEmpty({ message: 'Attendees is required.' })
  private attendees: string[];

  @Expose()
  @IsNotEmpty({ message: 'Subject is required.' })
  private subject: string;

  @Expose()
  @IsNotEmpty({ message: 'Content is required.' })
  private content: string;

  public deserialize(dto: any) {
    Object.assign(this, dto);
    return this;
  }

  public getAttendees(): string[] {
    return this.attendees;
  }

  public setAttendees(attendees: string[]): void {
    this.attendees = attendees;
  }

  public getSubject(): string {
    return this.subject;
  }

  public setSubject(subject: string): void {
    this.subject = subject;
  }

  public getContent(): string {
    return this.content;
  }

  public setContent(content: string): void {
    this.content = content;
  }
}
