import * as Agenda from 'agenda';
import { AppProperties } from 'src/Constant/app.properties';
import { AppPropertiesFactory } from 'src/Constant/app.properties.factory';

export class AgendaFactory {
    private static emailAgenda: any;

    public static getEmailAgenda() {
        if (AgendaFactory.emailAgenda == null) {
            AgendaFactory.emailAgenda = new Agenda({
                db: {
                    address: AgendaFactory.getURL(),
                    collection: AppPropertiesFactory.getProperties().jobsSchema,
                },
            });
        }
        return AgendaFactory.emailAgenda;
    }

    /**
     *
     * Initialize the Agenda Instance
     * @static
     * @memberof AgendaFactory
     */
    public static async initialize() {
        const agenda = AgendaFactory.getEmailAgenda();
        await new Promise((resolve) => agenda.once('ready', resolve));
        await this.emailAgenda.start();
    }

    private static getURL(): string {
        return (
            AppPropertiesFactory.getProperties().dbConfig.host +
            ':' +
            AppPropertiesFactory.getProperties().dbConfig +
            '/' +
            AppPropertiesFactory.getProperties().dbConfig.database
        );
    }
}
