import { validate } from 'class-validator';
import { ValidationException } from 'src/Core/Exception/ValidationException';

export class ValidationService {
  public static validateEntity(entity: any) {
    return new Promise(async (resolve, reject) => {
      validate(entity).then(errors => {
        if (errors.length > 0) {
          reject(new ValidationException('Validation Exception', errors));
        } else {
          resolve(null);
        }
      });
    });
  }
}
