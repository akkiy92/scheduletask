import { AppProperties } from './app.properties';
import * as fs from 'fs';
import { FileUtils } from 'src/Core/IO/file.utils';
import { plainToClass } from 'class-transformer';
import { LoggerFactory } from 'src/Core/Logger/logger';

export class AppPropertiesFactory {
  private static properties: AppProperties;
  private static logger = LoggerFactory.getLogger(AppPropertiesFactory);
  public static getProperties(): AppProperties {
    if (AppPropertiesFactory.properties == null) {
      try {
        const file =
          FileUtils.getSourceDir() +
          '/Assets/Properties/' +
          AppPropertiesFactory.getEnv() +
          '.json';
        const jsonData = JSON.parse(fs.readFileSync(file, 'utf-8'));
        AppPropertiesFactory.properties = plainToClass(AppProperties, jsonData);
      } catch (ex) {
        AppPropertiesFactory.logger.error(ex);
        throw new Error('Properties file not found.');
      }
    }
    return AppPropertiesFactory.properties;
  }

  private static getEnv() {
    return 'dev';
  }
}
