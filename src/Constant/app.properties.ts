import { DBProperties } from './db.properties';
import { EmailProperties } from './email.properties';

export class AppProperties {
    portNumber: number;
    dbConfig: DBProperties;
    jobsSchema: string;
    emailConfig: EmailProperties;
    apiUsername: string;
    apiPassword: string;
}
