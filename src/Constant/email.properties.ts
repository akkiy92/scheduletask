export class EmailProperties {
  host: string;
  port: string;
  username: string;
  password: string;
}
