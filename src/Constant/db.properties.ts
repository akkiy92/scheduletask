export class DBProperties {
  client: string;
  host: string;
  username: string;
  password: string;
  database: string;
  port: number;
}
