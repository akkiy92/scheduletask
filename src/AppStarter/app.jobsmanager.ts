import { AgendaFactory } from 'src/Modules/Agenda/agenda.factory';
import { EmailScheduleTaskService } from 'src/Modules/ScheduleTask/emailscheduletask.service';
import { AppContextManager } from 'src/Constant/app.context.manager';
import { EmailJobService } from 'src/Modules/Jobs/Email/emailjob.service';

export class AppJobsManager {
    private emailJobService: EmailJobService;
    private emailScheduleTaskService: EmailScheduleTaskService;
    constructor() {
        this.emailJobService = AppContextManager.app.get(EmailJobService);
        this.emailScheduleTaskService = AppContextManager.app.get(EmailScheduleTaskService);
    }

    public async resetJobs() {
        await AgendaFactory.initialize();
        await this.emailJobService.purgeAll();
        const tasks = await this.emailScheduleTaskService.getAllScheduleTask();
        for (let scheduletask of tasks) {
            this.emailJobService.addJob(
                scheduletask.getPattern(),
                scheduletask.getKey(),
                scheduletask.isIsRepeating(),
                scheduletask.getEmailContent()
            );
        }
    }
}
