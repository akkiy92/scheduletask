import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleTaskModule } from 'src/Modules/ScheduleTask/emailscheduletask.module';
import { AppPropertiesFactory } from 'src/Constant/app.properties.factory';
import { EmailService } from 'src/Modules/Email/email.service';
import { AgendaFactory } from 'src/Modules/Agenda/agenda.factory';
import { EmailScheduleTask } from 'src/Modules/ScheduleTask/emailscheduletask.entity';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mongodb',
            host: AppPropertiesFactory.getProperties().dbConfig.host,
            database: AppPropertiesFactory.getProperties().dbConfig.database,
            entities: [EmailScheduleTask],
            synchronize: true,
        }),
        ScheduleTaskModule,
    ],
    providers: [],
})
export class AppModule {
    constructor() {}
}
